#include "List.h"

Node* List::InsertNode(int index, string songTitle){
	Node* currNode = tempHead;
	Node* newNode = new Node;
	newNode->index = index;
	newNode->songTitle = songTitle;
	
	if (index == 0) {
		newNode->prev = NULL;
		newNode->next = head;
		head = newNode;
		tempHead = newNode;
	}
	else {
		newNode->prev = currNode;
		newNode->next = currNode->next;
		currNode->next = newNode;
		tempHead = newNode;
	}
	return newNode;
}

void List::DisplayPlaying(string songTitle){
	Node* currNode = head;
	Node* tempNode = head;
	while (currNode->songTitle != songTitle){
		currNode = currNode->next;
		if (currNode == NULL) {
			cout << "The song is not on the list.";
			break;
		}
	}
	if (currNode){
		cout << "Now Playing: \"" << currNode->songTitle << "\"\n";
		if (currNode->prev) {
			tempNode = currNode->prev;
			if (tempNode->songTitle == "head"){
				cout << "";
			}
			else {
			cout << "Previous:    \"" << tempNode->songTitle << "\"\n";
			}
		}
		if (currNode->next) {
			currNode = currNode->next;
			cout << "Next:        \"" << currNode->songTitle << "\"\n";
		}
	}
}

void List::DisplayAll(){
	int num = 0;
	Node* currNode = head;
	cout << "Song List\n";
	while (currNode != NULL) {
		if (currNode == head) {
			cout << "";
		}
		else {
			cout << num << ". " << currNode->songTitle << endl;
		}
		currNode = currNode->next;
		num++;
	}
}

void List::EditSong(string songTitle, string editTitle){
	Node* currNode = head;
	while (currNode->songTitle != songTitle) {
		currNode = currNode->next;
		if (currNode == NULL) {
			cout << "The song is not on the list.";
			break;
		}
	}
	if (currNode) {
		currNode->songTitle = editTitle;
	}
}

void List::DeleteSong(string songTitle) {
	Node* currNode = head;
	Node* tempNode = head;
	while (currNode->songTitle != songTitle) {
		currNode = currNode->next;
		tempNode = tempNode->next;
		if (currNode == NULL) {
			cout << "The song is not on the list.";
			break;
		}
	}
	if (currNode) {
		if (currNode->prev) {
			currNode->prev->next = currNode->next;
			delete currNode;
		}
		else {
			tempHead = currNode->next;
			delete currNode;
			head = tempHead;
		}
	}
}

string List::ReturnSongString(int index) {
	Node* currNode = head;
	while (currNode->index != index) {
		currNode = currNode->next;
		if (currNode == NULL) {
			cout << "The song is not on the list.";
			break;
		}
	}
	if (currNode) {
		return currNode->songTitle;
	}
	else {
		return NULL;
	}
}

Node* List::InsertInPlaylist(int index, string songTitle) {
	Node* currNode = head;
	Node* newNode = new Node;
	
	newNode->index = index;
	newNode->songTitle = songTitle;
    
    if (index == 0) {
    	newNode->prev = NULL;
        newNode->next = head;
        head = newNode;
    }
    else {
    	newNode->prev = currNode;
        newNode->next = currNode->next;
        currNode->next = newNode;
    }
    
	return newNode;
}

void List::Pop() {
	Node* currNode = head;
	currNode = currNode->next;
	if (currNode) {
		if (currNode->prev) {
			currNode->prev->next = currNode->next;
			head->next = currNode->prev->next;
			cout << currNode->songTitle << " has been removed from the playlist.\n\n";
			delete currNode;
		}
	}
	else {
		cout << "The list is empty.";
	}
}

void List::ViewPlayList() {
	int num = 0;
	Node* currNode = head;
	cout << "Your playlist\n";
	while (currNode != NULL) {
		if (currNode == head) {
			cout << "";
		}
		else {
			cout << num << ". " << currNode->songTitle << endl;
			cout << "";
		}
		currNode = currNode->next;
		num++;
	}
}

Node* List::Enqueue(int index, string songTitle){
	Node* currNode = tempHead;
	Node* newNode = new Node;
	newNode->index = index;
	newNode->songTitle = songTitle;
	
	if (index == 0) {
		newNode->prev = NULL;
		newNode->next = head;
		head = newNode;
		tempHead = newNode;
	}
	else {
		newNode->prev = currNode;
		newNode->next = currNode->next;
		currNode->next = newNode;
		tempHead = newNode;
	}
	return newNode;
}

void List::DisplayQueue() {
	int num = 0;
	Node* currNode = head;
	cout << "Your queue\n";
	while (currNode != NULL) {
		if (currNode == head) {
			cout << "";
		}
		else {
			cout << num << ". " << currNode->songTitle << endl;
		}
		currNode = currNode->next;
		num++;
	}
} 
