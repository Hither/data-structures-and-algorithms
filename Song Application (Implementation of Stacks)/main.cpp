#include <iostream>
#include "List.h"

string songTitle, editTitle, tempString;
int index = 10, ctr = 10, choice, nani, indexQ = 1, ctrQ = 0, search = 0, indexPL = 1, ctrPL = 0;
int GiveSomething(int choice, List list, List playlist, List queue);
char lChoice;

int main() {
	List list, playlist, queue;	
	
	//playlist
	list.InsertNode(0, "head");
	list.InsertNode(1, "Buwan by JK Labajo");
	list.InsertNode(2, "Islands by Reese Lansangan");
	list.InsertNode(3, "Mannequin by MilesExperience");
	list.InsertNode(4, "Ex by Callalily");
	list.InsertNode(5, "Pagtingin by Ben&Ben");
	list.InsertNode(6, "Ilaw sa Daan by IV of Spades");
	list.InsertNode(7, "Sandali Lang by Hulyo");
	list.InsertNode(8, "Wait by Over October");
	list.InsertNode(9, "ayaw by Syd Hartha");
	list.InsertNode(10, "Love+U by Jensen and the Flips");
	queue.InsertNode(0, "head");
	playlist.InsertInPlaylist(0, "head");
	
	cout << "Welcome to my Music Application!\n";
	cout << "What do you want to do?\n";
	
	//this is where the options will be reiterated
	while (choice != 12) {
		cout << "\n1: Insert a song\n2: Play a Song\n3: Edit a Song\n4: Delete a Song\n5: View All Songs";
		cout << "\n6: Add in Playlist\n7: View Playlist\n8: Remove in Playlist \n9: Add to Queue\n10: Dequeue\n11: View Queue\n";
		cout << "12: Exit\n>> ";
		cin >> choice;
		nani = GiveSomething(choice, list, playlist, queue);
		//this is for adding songs in the library
		while (nani == 1) {
			if (ctr != 0) {
				index = ctr + 1;
			}
			while(lChoice != 'N') {
				system("CLS");
				cout << "Song Title: ";
				cin.get();
				getline(cin, songTitle);
				list.InsertNode(index, songTitle); //calls insertnode from the list class
				index++;
				cout << "Continue inputing songs? (Y/N): ";
				cin >> lChoice;
				ctr = index;
			}
			lChoice = 'Y';
			nani = 0;
		}
		//this is for adding songs in the playlist/stack
		while (nani == 6){
			if (ctrPL != 0) {
				indexPL = ctrPL;
			}
			while(lChoice != 'N') {
				system("CLS");
				list.DisplayAll();
				cout << "Song Index: ";
				cin >> search;
				playlist.InsertInPlaylist(indexPL, list.ReturnSongString(search)); //calls insertnode from the list class
				indexPL++;
				cout << "Continue adding songs to your queue? (Y/N): ";
				cin >> lChoice;
				ctrPL = indexPL;
			}
			lChoice = 'Y';
			nani = 0;
		}
		// this is for adding songs in the queue
		while (nani == 9){
			if (ctrQ != 0) {
				indexQ = ctrQ;
			}
			while(lChoice != 'N') {
				system("CLS");
				list.DisplayAll();
				cout << "Song Index: ";
				cin >> search;
				queue.Enqueue(indexQ, list.ReturnSongString(search)); //calls insertnode from the list class
				indexQ++;
				cout << "Continue adding songs to your queue? (Y/N): ";
				cin >> lChoice;
				ctrQ = indexQ;
			}
			lChoice = 'Y';
			nani = 0;
		}
	}
	return 0;
}

//a function for the options
int GiveSomething(int choice, List list, List playlist, List queue){
	index = 0;
	system("CLS");
	switch (choice) 
	{
		case 1:
			return 1;
			break;
		case 2:
			list.DisplayAll();
			cout << "\nTitle of the song you want to play: ";
			cin.get();
			getline(cin, songTitle);
			system("CLS");
			list.DisplayPlaying(songTitle);
			return 2;
			break;
		case 3:
			list.DisplayAll();
			cout << "\nTitle of the song you want to edit: ";
			cin.get();
			getline(cin, songTitle);
			cout << "To: ";
			getline(cin, editTitle);
			list.EditSong(songTitle, editTitle);
			return 3;
			break;
		case 4:
			list.DisplayAll();
			cout << "\nTitle of the song you want to delete: ";
			cin.get();
			getline(cin, songTitle);
			list.DeleteSong(songTitle);
			return 4;
			break;
		case 5:
			list.DisplayAll();
			return 5;
			break;
		case 6:
			return 6;
			break;
		case 7:
			playlist.ViewPlayList();
			return 7;
			break;
		case 8:
			playlist.Pop();
			playlist.ViewPlayList();
			return 8;
			break;
		case 9:	
			return 9;
			break;
		case 10:
			queue.Pop();
			queue.DisplayQueue();
			return 10;
			break;
		case 11:
			queue.DisplayQueue();
			return 11;
			break;
		case 12:
			return 12;
			break;
	}
}
