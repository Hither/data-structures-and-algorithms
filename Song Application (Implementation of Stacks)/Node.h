#ifndef NODE_H
#define NODE_H
#include <iostream>
using namespace std;

class Node
{
	public:
		int index;
		string songTitle;
		
		Node* prev;
		Node* next;
};

#endif
