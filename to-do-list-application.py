to_do_list = []
ctr = 0

print("Welcome to your to-do list application!")

while ctr != 1:
    print("What do you want to do?")
    print("1: Input a task")
    print("2: Remove a task")
    print("3: View to-do list")
    print("4: Exit")
    start = input(">> ")
    ctr += 1

    if start == "1":
        task = input("What is your task?\n>> ")
        to_do_list.append(task)
        print("Your task has been added to the to-do list!")
        print("-------------------------------------------")
        ctr = 0
    
    elif start == "2":
        if len(to_do_list) != 0:
            print("Your to-do list")
            for i in range(len(to_do_list)):
                print(str(to_do_list.index(to_do_list[i]) + 1) + ". " + to_do_list[i])
            tr = input("What is number of the task you want to delete?\n>> ")
            tr = int(tr) - 1
            to_do_list.remove(to_do_list[tr])
            print("The task has been removed!")
            print("-------------------------------------------")
        else:
            print("Your to-do list is empty!")
            print("-------------------------------------------")
        ctr = 0
    
    elif start == "3":
        if len(to_do_list) != 0:
            print("Your to-do list")
            for i in range(len(to_do_list)):
                print(str(to_do_list.index(to_do_list[i]) + 1) + ". " + to_do_list[i])
        else:
            print("Your to-do list is empty!")
        print("-------------------------------------------")
        ctr = 0
    
    elif start == "4":
        print("Thank you for using this application!")
        print("Made by John Lloyd P. Demotica")
        print("BSCPE 2-3")
        
        ctr = 1