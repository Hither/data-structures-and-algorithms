#include <iostream>
#include "List.h"
#include <fstream>
#include <windows.h>

string songTitle, editTitle, tempString, songGenre, songAlbum, songArtist, tempGenre;
int index = 10, ctr = 10, choice, nani, indexQ = 1, ctrQ = 0, search = 0, indexPL = 1, ctrPL = 0, i = 1, genre;
int GiveSomething(int choice, List list, List playlist, List queue);
char lChoice;

int main() {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // for color
	List list, playlist, queue;
	ifstream songTitlesFile;
	ifstream artistsFile;
	ifstream albumsFile;
	ifstream songGenresFile;
	ifstream songGenreSorterFile;
	
	songTitlesFile.open("SongTitles.txt");
	artistsFile.open("Artists.txt");
	songGenresFile.open("SongGenre.txt");
	albumsFile.open("Albums.txt");
	songGenreSorterFile.open("SongGenreSort.txt");
	//playlist
	list.InsertNode(0, "head", "", "", "", 0);

	//this is where the program will read the data from the text file
	while(getline(songTitlesFile, songTitle)) {
		getline(artistsFile, songArtist);
		getline(songGenresFile, songGenre);
		getline(albumsFile, songAlbum);
		getline(songGenreSorterFile, tempGenre);
		list.InsertNode(i, songTitle, songArtist, songGenre, songAlbum, stoi(tempGenre));
		i++;
	}

	songTitlesFile.close();
	artistsFile.close();
	songGenresFile.close();
	albumsFile.close();
	songGenreSorterFile.close();

	queue.InsertNode(0, "head", "", "", "", 0);
	
	playlist.InsertInPlaylist(0, "head");
		SetConsoleTextAttribute(hConsole, 10);
	
	cout << "  _      __    __                     __                             \n";
	cout << " | | /| / /__ / /______  __ _  ___   / /____                         \n";
	cout << " | |/ |/ / -_) / __/ _ \\/  ' \\/ -_) / __/ _ \\                        \n";
	cout << " |__/|__/\\__/_/\\__/\\___/_/_/_/\\__/  \\__/\\___/___  __                 \n";
	cout << "       __ _  __ __  /  |/  /_ _____ (_)___  / _ \\/ /__ ___ _____ ____\n";
	cout << "      /  ' \\/ // / / /|_/ / // (_-</ / __/ / ___/ / _ `/ // / -_) __/\n";
	cout << "     /_/_/_/\\_, / /_/  /_/\\_,_/___/_/\\__/ /_/  /_/\\_,_/\\_, /\\__/_/   \n";
	cout << "           /___/                                      /___/          \n";
	cout << "What do you want to do?\n";
	
	//this is where the options will be reiterated
	while (choice != 13) {

		cout << "\n1: Insert a song\n2: Play a Song\n3: Edit a Song\n4: Delete a Song\n5: View All Songs";
		cout << "\n6: Add in Playlist\n7: View Playlist\n8: Remove in Playlist \n9: Add to Queue\n10: Dequeue\n11: View Queue\n";
		cout << "12: Sort Album by Genre\n13: Exit\n>> ";
		cin >> choice;
		nani = GiveSomething(choice, list, playlist, queue);
		//this is for adding songs in the library
		while (nani == 1) {
			if (ctr != 0) {
				index = ctr + 1;
			}
			while(lChoice != 'N') {
				system("CLS");
				cout << "Song Title: ";
				cin.get();
				getline(cin, songTitle);
				list.InsertNode(index, songTitle, songArtist, songGenre, songAlbum, genre); //calls insertnode from the list class
				index++;
				cout << "Continue inputing songs? (Y/N): ";
				cin >> lChoice;
				ctr = index;
			}
			lChoice = 'Y';
			nani = 0;
		}
		//this is for adding songs in the playlist/stack
		while (nani == 6){
			if (ctrPL != 0) {
				indexPL = ctrPL;
			}
			while(lChoice != 'N') {
				system("CLS");
				list.DisplayAll();
				cout << "Song Index: ";
				cin >> search;
				playlist.InsertInPlaylist(indexPL, list.ReturnSongString(search)); //calls insertnode from the list class
				indexPL++;
				cout << "Continue adding songs to your queue? (Y/N): ";
				cin >> lChoice;
				ctrPL = indexPL;
			}
			lChoice = 'Y';
			nani = 0;
		}
		// this is for adding songs in the queue
		while (nani == 9){
			if (ctrQ != 0) {
				indexQ = ctrQ;
			}
			while(lChoice != 'N') {
				system("CLS");
				list.DisplayAll();
				cout << "Song Index: ";
				cin >> search;
				queue.Enqueue(indexQ, list.ReturnSongString(search)); //calls insertnode from the list class
				indexQ++;
				cout << "Continue adding songs to your queue? (Y/N): ";
				cin >> lChoice;
				ctrQ = indexQ;
			}
			lChoice = 'Y';
			nani = 0;
		}
	}
	return 0;
}

//a function for the options
int GiveSomething(int choice, List list, List playlist, List queue){
	index = 0;
	system("CLS");
	switch (choice) 
	{
		//returns 1 for adding songs
		case 1:
			return 1;
			break;
		//this is where you'll type the title of the song that you wish to play
		case 2:
			list.DisplayAll();
			cout << "\nTitle of the song you want to play: ";
			cin.get();
			getline(cin, songTitle);
			system("CLS");
			list.DisplayPlaying(songTitle);
			return 2;
			break;
		//this is where you'll type the title of the song that you want to edit from the song library
		case 3:
			list.DisplayAll();
			cout << "\nTitle of the song you want to edit: ";
			cin.get();
			getline(cin, songTitle);
			cout << "To: ";
			getline(cin, editTitle);
			list.EditSong(songTitle, editTitle);
			return 3;
			break;
		//this is where the songs will be deleted
		case 4:
			list.DisplayAll();
			cout << "\nTitle of the song you want to delete: ";
			cin.get();
			getline(cin, songTitle);
			list.DeleteSong(songTitle);
			return 4;
			break;
		//in this case, the library will display all the songs
		case 5:
			list.DisplayAll();
			return 5;
			break;
		//this will return 6 for adding to playlist
		case 6:
			return 6;
			break;
		//in this case, you'll view your own playlist
		case 7:
			playlist.ViewPlayList();
			return 7;
			break;
		//this is where you'll remove your list in the playlist
		case 8:
			playlist.Pop();
			playlist.ViewPlayList();
			return 8;
			break;
		//this will return 9 for adding to your queue
		case 9:	
			return 9;
			break;
		//this will remove the queue
		case 10:
			queue.Pop();
			queue.DisplayQueue();
			return 10;
			break;
		//this will display all the items in your queue
		case 11:
			queue.DisplayQueue();
			return 11;
			break;
		//exit
		case 12:
			list.sortByAlbum();
			break;
		case 13:
			return 13;
			break;
	}
}
