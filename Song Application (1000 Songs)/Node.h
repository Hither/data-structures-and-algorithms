#ifndef NODE_H
#define NODE_H
#include <iostream>
using namespace std;

class Node
{
	public:
		int index;
		string songTitle;
		string songArtist;
		string songAlbum;
		string songGenre;
		int genreSort;
		
		Node* prev;
		Node* next;
};

#endif
