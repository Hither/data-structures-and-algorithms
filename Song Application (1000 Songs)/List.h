#ifndef LIST_H
#define LIST_H
#include <cstddef>
#include "Node.h"

class List
{
	public:
		List() { head = NULL; } //constructor
		void InsertNode(int index, string songTitle, string songArtist, string songGenre, string album, int genreSort);
		void DisplayPlaying(string songTitle);
		void DisplayAll();
		void EditSong(string songTitle, string editTitle);
		void DeleteSong(string songTitle);
		string ReturnSongString(int index);
		Node* InsertInPlaylist(int index, string songTitle);
		void Pop();
		void ViewPlayList();
		Node* Enqueue(int index, string songTitle);
		void DisplayQueue();
		void sortByAlbum();
		
	private:
		Node* head;
		Node* tempHead;
};

#endif
