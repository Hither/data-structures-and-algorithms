#ifndef LIST_H
#define LIST_H
#include <cstddef>
#include "Node.h"

class List
{
	public:
		List() { head = NULL; }
		
		Node* InsertNode(int index, string movieName, string director, string producer, string cast, bool isAvailable);
		void DisplayList();
		void DisplayMovieDetails(string movieName);
		int RentDVD(string movieName);
		int ReturnDVD(string movieName);
		int DisplayUnavailable();
	private:
		Node* head;
};

#endif
