#ifndef NODE_H
#define NODE_H
#include <iostream>
using namespace std;

class Node
{
	public:
		string movieName;
		string director;
		string producer;
		string cast;
		bool isAvailable;

		Node* next;
};

#endif
