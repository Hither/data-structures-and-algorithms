#include "List.h"

Node* List::InsertNode(int index, string movieName, string director, string producer, string cast, bool isAvailable) {
	if (index < 0) return NULL;
	
	int currIndex = 1;
	Node* currNode = head;
	while (currNode && index > currIndex) {
		currNode = currNode->next;
		currIndex++;
	}
	if (index > 0 && currNode == 0) return NULL;
	
	Node* newNode = new Node;
	newNode->movieName = movieName;
	newNode->director = director;
	newNode->producer = producer;
	newNode->cast = cast;
	newNode->isAvailable = isAvailable;
	
	if (index == 0) {
		newNode->next = head;
		head = newNode;
	}	
	else {
		newNode->next = currNode->next;
		currNode->next = newNode;
	}
	return newNode;
}


void List::DisplayList() {
	int num = 1;
	Node* currNode = head;
	cout << "Movie List\n";
	while(currNode != NULL) {
		cout << num << ". "<< currNode->movieName;
		if (!currNode->isAvailable) cout << "(Unavailable)\n";
		else cout << "\n";
		currNode = currNode->next;
		num++;
	}
	cout << endl;
}
	
void List::DisplayMovieDetails(string movieName) {
	Node* currNode = head;
	while (currNode && currNode->movieName != movieName) {
		currNode = currNode->next;
	}
	if (currNode) {
		cout << "Director: " << currNode->director << endl;
		cout << "Producer: " << currNode->producer << endl;
		cout << "Cast: "     << currNode->cast << endl;
		cout << "Availability: ";
		if(currNode->isAvailable) cout << "Available\n";
		else cout << "Unavailable\n";
	}
	else cout << "The movie is not on the list.\n";
}

int List::RentDVD(string movieName){
	Node* currNode = head;
	while (currNode && currNode->movieName != movieName) {
		currNode = currNode->next;
	}
	if (currNode){
		if (currNode->isAvailable) {
			currNode->isAvailable = false;
			cout << "The movie " << currNode->movieName << " is now unavailable.\n";
		}
		else cout << "The movie is unavailable.";
	}
	else {
		cout << "The movie is not on the list.\n";
		return 1;
	}
}

int List::ReturnDVD(string movieName){
	Node* currNode = head;
	while (currNode && currNode->movieName != movieName) {
		currNode = currNode->next;
	}
	if (currNode) {
		if (!currNode->isAvailable) {
			currNode->isAvailable = true;
			cout << "The movie " << currNode->movieName << " is now available.\n";
		}
		else cout << "The movie is available.\n";
	}
	else {
		cout << "The movie is not on the list.\n";
		return 1;
	}
}

int List::DisplayUnavailable(){
	int ctr;
	Node* currNode = head;
	cout << "Movies Rented: \n";
	while(currNode != NULL) {
		if (!currNode->isAvailable){
			cout << currNode->movieName << endl;
			ctr++;
		}
		currNode = currNode->next;
	}
	return ctr;
}
