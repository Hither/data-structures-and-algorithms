#include <windows.h>
#include "List.h"

string title, dateRented;
int choice, qtt;

int giveSomething(List list, string fName, string lName, int choice);
string giveName(string whatName);
int options();

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // for color

int main() {
	List list; // creating the list: index, title, director, producer, cast, availability
	list.InsertNode(0, "Finding Nemo", "Andrew Stanton", "Graham Walters", "Alexander Gould, Albert Brooks, Ellen DeGeneres", true);
	list.InsertNode(1, "The Lion King", "Jon Favreau", "Jeffrey Silver", "Donald Glover, Beyonce", true);
	list.InsertNode(2, "Toy Story", "John Lasseter", "Tom Hanks", "Tom Hanks, Tim Allen", true);
	list.InsertNode(3, "Brother Bear", "Robert Walker, Aaron Blaise", "Allison Grodner", "Rick Moranis, Jeremy Suarez, Michael Clark Duncan", true);
	list.InsertNode(4, "Cars", "Joe Ranft, John Lasseter", "Darla K. Anderson", "Owen Wilson, Larry the Cable Guy, Bonnie Hunt", true);
	list.InsertNode(5, "Iron Man", "Jon Favreau", "Kevin Feige, Avi Arad", "Robert Downey Jr., Jon Favreau, Gwyneth Paltrow", true);
	list.InsertNode(6, "The Incredible Hulk", "Louis Leterrier", "Kevin Feige, Gale Anne Hurd, Avi Arad", "Edward Norton, Liv Tyler, Lou Ferrigno",  true);
	list.InsertNode(7, "Iron Man 2", "Jon Favreau", "Kevin Feige", "Robert Downey Jr., Don Cheadle, Scarlet Johansson", true);
	list.InsertNode(8, "Thor", "Kenneth Branagh", "Kevin Feige", "Chris Hemsworth, Natalie Portman, Tom Hiddleston", true);
	list.InsertNode(9, "Captain America The First Avenger", "Joe Johnston", "Kevin Feige", "Chris Evans, Hayley Atwell, Sebastian Stan", true);
	list.InsertNode(10, "The Avengers", "Joss Whedon", "Kevin Feige", "Robert Downey Jr., Chirs Evans, Chris Hemsworth, Mark Rufallo",  true);
	list.InsertNode(11, "Iron Man 3", "Shane Black", "Kevin Feige", "Robert Downey Jr., Scarlet Johansson, Gwyneth Paltrow", true);
	list.InsertNode(12, "Thor The Dark World", "Alan Taylor", "Kevin Feige", "Chris Hemsworth, Tom Hiddleston, Natalie Portman", true);
	list.InsertNode(13, "Captain America The Winter Soldier", "Anthony Russo, Joe Russo", "Kevin Feige", "Chris Evans, Scarlet Johansson, Sebastian Stan", true);
	list.InsertNode(14, "Guardians of the Galaxy", "James Gunn", "Kevin Feige", "Chris Pratts, Zoe Saldana, Vin Diesel", true);
	list.InsertNode(15, "Avengers Age of Ultron", "Joss Whedon", "Kevin Feige", "Robert Downey Jr., Chris Evans, Chris Hemsworth, Mark Rufallo", true);
	list.InsertNode(16, "Ant-Man", "Peyton Reed", "Kevin Feige", "Paul Rudd, Evangeline Lilly, Michael Douglas, Michael Pena", true);
	list.InsertNode(17, "Captain America The Civil War", "Anthony Russo, Joe Russo", "Kevin Feige", "Robert Downey Jr., Chris Evans, Scarlet Johansson, Anthony Mackie", true);
	list.InsertNode(18, "Doctor Strange", "Scott Derickson", "Kevin Feige", "Benedict Cumberbatch, Tilda Swinton, Benedict Wong", true);
	list.InsertNode(19, "Guardians of the Galaxy Vol. 2", "James Gunn", "Kevin Feige", "Chris Pratts, Dave Bautista, Zoe Saldana, Vin Diesel", true);
	list.InsertNode(20, "Spider-Man Homecoming", "Jon Watts", "Kevin Feige, Amy Pascal", "Tom Holland, Robert Downey Jr., Jon Favreau", true);
	list.InsertNode(21, "Thor Ragnarok", "Taika Waititi", "Kevin Feige", "Chris Hemsworth, Tom Hiddleston, Tessa Thompson", true);
	list.InsertNode(22, "Black Panther", "Ryan Coogler", "Kevin Feige", "Chadwick Boseman, Danai Guirira, Letitia Wright, Michael Jordan", true);
	list.InsertNode(23, "Avengers Infinity War", "Anthony Russo, Joe Russo", "Kevin Feige", "Robert Downey Jr., Chris Evans, Chris Hemsworth, Scarlet Johansson, Mark Rufallo", true);
	list.InsertNode(24, "Ant-Man and the Wasp", "Peyton Reed", "Kevin Feige, Stephen Broussard", "Paul Rudd, Evangeline Lilly, Michael Douglas, Michael Pena", "yes");

	SetConsoleTextAttribute(hConsole, 10);
	cout << " __          __  _                            _           ____              \n";
	cout << " \\ \\        / / | |                          | |         / __ \\             \n";
	cout << "  \\ \\  /\\  / /__| | ___ ___  _ __ ___   ___  | |_ ___   | |  | |_   _ _ __  \n";
	cout << "   \\ \\/  \\/ / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\ | __/ _ \\  | |  | | | | | '__| \n";
	cout << "    \\  /\\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | | |__| | |_| | |    \n";
	cout << "     \\/ _\\/_\\___|_|\\___\\___/|_| |_|_|_|\\___|  \\__\\___/   \\____/ \\__,_|_|    \n";
	cout << "       |  __ \\ \\    / /  __ \\   / ____|         | |                         \n";
	cout << "       | |  | \\ \\  / /| |  | | | (___  _   _ ___| |_ ___ _ __ ___           \n";
	cout << "       | |  | |\\ \\/ / | |  | |  \\___ \\| | | / __| __/ _ \\ '_ ` _ \\          \n";
	cout << "       | |__| | \\  /  | |__| |  ____) | |_| \\__ \\ ||  __/ | | | | |         \n";
	cout << "       |_____/   \\/   |_____/  |_____/ \\__, |___/\\__\\___|_| |_| |_|         \n";
	cout << "                                        __/ |                               \n";
	cout << "                                       |___/                                \n";
	
	cout << "How can I help you?\n";
	cout << "1: Browse the options...\n2: Exit\n>> ";
	cin >> choice;
	system("CLS");
	
	cout << "Fill up first the information...\n";
	string fName = giveName("First");
	string lName = giveName("Last");
	cout << "Date of transaction: ";
	cin.get();
	getline(cin, dateRented);
	system("CLS");
	
	while(choice == 1){	
		cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		cout << "+       _____   _____  _______ _____  _____  __   _ _______       +\n";
		cout << "+      |     | |_____]    |      |   |     | | \\  | |______       +\n";
		cout << "+      |_____| |          |    __|__ |_____| |  \\_| ______|       +\n";
		cout << "+                                                                 +\n";
		cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		cout << "1: Rent a DVD\n2: Return a DVD\n3: Search for a DVD\n4: Show all DVDs\n5: Cart\n6: Exit\n>> ";
		cin >> choice;
		system("CLS");
		choice = giveSomething(list, fName, lName, choice);
	}
	if (choice == 0) {
		cout << "Movies x" << qtt << " = " << 50*qtt << endl; 
		cout << "Please pay P" << 50*qtt << " at the counter.\n";
	}
	
	cout << "\n$$$$$$$$\\ $$\\                           $$\\                                           $$\\\n";
	cout << "\\__$$  __|$$ |                          $$ |                                          $$ |\n";
	cout << "   $$ |   $$$$$$$\\   $$$$$$\\  $$$$$$$\\  $$ |  $$\\       $$\\   $$\\  $$$$$$\\  $$\\   $$\\ $$ |\n";
	cout << "   $$ |   $$  __$$\\  \\____$$\\ $$  __$$\\ $$ | $$  |      $$ |  $$ |$$  __$$\\ $$ |  $$ |$$ |\n";
	cout << "   $$ |   $$ |  $$ | $$$$$$$ |$$ |  $$ |$$$$$$  /       $$ |  $$ |$$ /  $$ |$$ |  $$ |\\__|\n";
	cout << "   $$ |   $$ |  $$ |$$  __$$ |$$ |  $$ |$$  _$$<        $$ |  $$ |$$ |  $$ |$$ |  $$ |    \n";
	cout << "   $$ |   $$ |  $$ |\\$$$$$$$ |$$ |  $$ |$$ | \\$$\\       \\$$$$$$$ |\\$$$$$$  |\\$$$$$$  |$$\\\n ";
	cout << "  \\__|   \\__|  \\__| \\_______|\\__|  \\__|\\__|  \\__|       \\____$$ | \\______/  \\______/ \\__|\n";
    cout << "                                                        $$\\   $$ |                        \n";
    cout << "                                                        \\$$$$$$  |      \n"                  ;
    cout << "                                                         \\______/          \n";               
	cout << "Thank you for using our service!\nCome again next time!";
	return 0;
}

int giveSomething(List list, string fName, string lName, int choice){
	string movieName;
	switch (choice) {
		case 1:
			list.DisplayList();
			cout << "Title of Movie: ";
			cin.get();
			getline(cin, movieName);
			system("CLS");
			if (list.RentDVD(movieName) != 1){
				list.DisplayList();
			}
			break;
		case 2:
			list.DisplayList();
			cout << "Title of Movie: ";
			cin.get();
			getline(cin, movieName);
			system("CLS");
			if (list.ReturnDVD(movieName) != 1) {
				list.DisplayList();
			}
			break;
		case 3:
			cout << "Title of Movie: ";
			cin.get();
			getline(cin, movieName);
			list.DisplayMovieDetails(movieName);
			break;
		case 4:
			list.DisplayList();
			break;
		case 5:
			cout << "Customer: " << fName << " " << lName << endl;
			cout << "Date of Transaction: " << dateRented << endl;
			qtt = list.DisplayUnavailable();
			cout << "0: Pay-out: ";
			break;
		case 6:
			return 2;
			break;
		default:
			cout << "Invalid input.";
			break;
	}
	return options();
}

string giveName(string whatName){
	string name;
	cout << whatName << " name: ";
	cin >> name;
	return name;
}

int options(){
	int choice;
	cout << "\n1: Return to options\n2: Exit\n>> ";
	cin >> choice;
	system("CLS");
	return choice;
}
