#include <iostream>
#include <windows.h>
using namespace std;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	//title, director, producer, cast, availability
string movies[25][5] = {
	{"Finding Nemo", "Andrew Stanton", "Graham Walters", "Alexander Gould, Albert Brooks, Ellen DeGeneres", "yes"},
	{"The Lion King", "Jon Favreau", "Jeffrey Silver", "Donald Glover, Beyonce", "yes"},
	{"Toy Story", "John Lasseter", "Tom Hanks", "Tom Hanks, Tim Allen", "yes"},
	{"Brother Bear", "Robert Walker, Aaron Blaise", "Allison Grodner", "Rick Moranis, Jeremy Suarez, Michael Clark Duncan", "yes"},
	{"Cars", "Joe Ranft, John Lasseter", "Darla K. Anderson", "Owen Wilson, Larry the Cable Guy, Bonnie Hunt", "yes"},
	{"Iron Man", "Jon Favreau", "Kevin Feige, Avi Arad", "Robert Downey Jr., Jon Favreau, Gwyneth Paltrow", "yes"},
	{"The Incredible Hulk", "Louis Leterrier", "Kevin Feige, Gale Anne Hurd, Avi Arad", "Edward Norton, Liv Tyler, Lou Ferrigno",  "yes"},
	{"Iron Man 2", "Jon Favreau", "Kevin Feige", "Robert Downey Jr., Don Cheadle, Scarlet Johansson", "yes"},
	{"Thor", "Kenneth Branagh", "Kevin Feige", "Chris Hemsworth, Natalie Portman, Tom Hiddleston", "yes"},
	{"Captain America The First Avenger", "Joe Johnston", "Kevin Feige", "Chris Evans, Hayley Atwell, Sebastian Stan", "yes"},
	{"The Avengers", "Joss Whedon", "Kevin Feige", "Robert Downey Jr., Chirs Evans, Chris Hemsworth, Mark Rufallo",  "yes"},
	{"Iron Man 3", "Shane Black", "Kevin Feige", "Robert Downey Jr., Scarlet Johansson, Gwyneth Paltrow", "yes"},
	{"Thor The Dark World", "Alan Taylor", "Kevin Feige", "Chris Hemsworth, Tom Hiddleston, Natalie Portman", "yes"},
	{"Captain America The Winter Soldier", "Anthony Russo, Joe Russo", "Kevin Feige", "Chris Evans, Scarlet Johansson, Sebastian Stan", "yes"},
	{"Guardians of the Galaxy", "James Gunn", "Kevin Feige", "Chris Pratts, Zoe Saldana, Vin Diesel", "yes"},
	{"Avengers Age of Ultron", "Joss Whedon", "Kevin Feige", "Robert Downey Jr., Chris Evans, Chris Hemsworth, Mark Rufallo", "yes"},
	{"Ant-Man", "Peyton Reed", "Kevin Feige", "Paul Rudd, Evangeline Lilly, Michael Douglas, Michael Pena", "yes"},
	{"Captain America The Civil War", "Anthony Russo, Joe Russo", "Kevin Feige", "Robert Downey Jr., Chris Evans, Scarlet Johansson, Anthony Mackie", "yes"},
	{"Doctor Strange", "Scott Derickson", "Kevin Feige", "Benedict Cumberbatch, Tilda Swinton, Benedict Wong", "yes"},
	{"Guardians of the Galaxy Vol. 2", "James Gunn", "Kevin Feige", "Chris Pratts, Dave Bautista, Zoe Saldana, Vin Diesel", "yes"},
	{"Spider-Man Homecoming", "Jon Watts", "Kevin Feige, Amy Pascal", "Tom Holland, Robert Downey Jr., Jon Favreau", "yes"},
	{"Thor Ragnarok", "Taika Waititi", "Kevin Feige", "Chris Hemsworth, Tom Hiddleston, Tessa Thompson", "yes"},
	{"Black Panther", "Ryan Coogler", "Kevin Feige", "Chadwick Boseman, Danai Guirira, Letitia Wright, Michael Jordan", "yes"},
	{"Avengers Infinity War", "Anthony Russo, Joe Russo", "Kevin Feige", "Robert Downey Jr., Chris Evans, Chris Hemsworth, Scarlet Johansson, Mark Rufallo", "yes"},
	{"Ant-Man and the Wasp", "Peyton Reed", "Kevin Feige, Stephen Broussard", "Paul Rudd, Evangeline Lilly, Michael Douglas, Michael Pena", "yes"}
	};

string title, dateRented;
int choice, ctr, flag, qtt;
bool onTheList;
int options();
string giveName(string whatName);
int giveSomething(int choice, string fName, string lName);

int main(){
	SetConsoleTextAttribute(hConsole, 10);
	cout << " __          __  _                            _           ____              \n";
	cout << " \\ \\        / / | |                          | |         / __ \\             \n";
	cout << "  \\ \\  /\\  / /__| | ___ ___  _ __ ___   ___  | |_ ___   | |  | |_   _ _ __  \n";
	cout << "   \\ \\/  \\/ / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\ | __/ _ \\  | |  | | | | | '__| \n";
	cout << "    \\  /\\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | | |__| | |_| | |    \n";
	cout << "     \\/ _\\/_\\___|_|\\___\\___/|_| |_|_|_|\\___|  \\__\\___/   \\____/ \\__,_|_|    \n";
	cout << "       |  __ \\ \\    / /  __ \\   / ____|         | |                         \n";
	cout << "       | |  | \\ \\  / /| |  | | | (___  _   _ ___| |_ ___ _ __ ___           \n";
	cout << "       | |  | |\\ \\/ / | |  | |  \\___ \\| | | / __| __/ _ \\ '_ ` _ \\          \n";
	cout << "       | |__| | \\  /  | |__| |  ____) | |_| \\__ \\ ||  __/ | | | | |         \n";
	cout << "       |_____/   \\/   |_____/  |_____/ \\__, |___/\\__\\___|_| |_| |_|         \n";
	cout << "                                        __/ |                               \n";
	cout << "                                       |___/                                \n";
	cout << "How can I help you?\n";
	cout << "1: Browse the options...\n2: Exit\n>> ";
	cin >> choice;
	system("CLS");
	cout << "Fill up first the information...\n";
	string fName = giveName("First");
	string lName = giveName("Last");
	cout << "Date of transaction: ";
	cin.get();
	getline(cin, dateRented);
	system("CLS");
	while(choice == 1){	
		cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		cout << "+       _____   _____  _______ _____  _____  __   _ _______       +\n";
		cout << "+      |     | |_____]    |      |   |     | | \\  | |______       +\n";
		cout << "+      |_____| |          |    __|__ |_____| |  \\_| ______|       +\n";
		cout << "+                                                                 +\n";
		cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		cout << "1: Rent a DVD\n2: Return a DVD\n3: Search for a DVD\n4: Show all DVDs\n5: Cart\n6: Exit\n>> ";
		cin >> choice;
		system("CLS");
		choice = giveSomething(choice, fName, lName);
	}
	if (choice == 0){
		cout << "Movies x" << qtt << " = " << 50*qtt << endl; 
		cout << "Please pay P" << 50*qtt << " at the counter.\n";
	}
	cout << "\n$$$$$$$$\\ $$\\                           $$\\                                           $$\\\n";
	cout << "\\__$$  __|$$ |                          $$ |                                          $$ |\n";
	cout << "   $$ |   $$$$$$$\\   $$$$$$\\  $$$$$$$\\  $$ |  $$\\       $$\\   $$\\  $$$$$$\\  $$\\   $$\\ $$ |\n";
	cout << "   $$ |   $$  __$$\\  \\____$$\\ $$  __$$\\ $$ | $$  |      $$ |  $$ |$$  __$$\\ $$ |  $$ |$$ |\n";
	cout << "   $$ |   $$ |  $$ | $$$$$$$ |$$ |  $$ |$$$$$$  /       $$ |  $$ |$$ /  $$ |$$ |  $$ |\\__|\n";
	cout << "   $$ |   $$ |  $$ |$$  __$$ |$$ |  $$ |$$  _$$<        $$ |  $$ |$$ |  $$ |$$ |  $$ |    \n";
	cout << "   $$ |   $$ |  $$ |\\$$$$$$$ |$$ |  $$ |$$ | \\$$\\       \\$$$$$$$ |\\$$$$$$  |\\$$$$$$  |$$\\\n ";
	cout << "  \\__|   \\__|  \\__| \\_______|\\__|  \\__|\\__|  \\__|       \\____$$ | \\______/  \\______/ \\__|\n";
    cout << "                                                        $$\\   $$ |                        \n";
    cout << "                                                        \\$$$$$$  |      \n"                  ;
    cout << "                                                         \\______/          \n";               
	cout << "Thank you for using our service!\nCome again next time!";
	return 0;
}

int giveSomething(int choice, string fName, string lName){
	ctr = 0;
	if (choice == 1){
		cout << "Movie list\n";
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ". " << movies[i][0];
			if (movies[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Unavailable)\n";
			}
		}
		cout << "\nTitle of the movie: ";
		cin.get();
		getline(cin, title);
		for (int i = 0; i <= 24; i++){
			if(movies[i][0] == title){
				movies[i][4] = "no";
				onTheList = true;
				ctr = i;
			}
		}
		if (onTheList == 1){
			cout << "The movie " << movies[ctr][0];
			if(movies[ctr][4] == "no" && flag == 1){
			cout << " is unavailable.\n";
			}
			else{
			cout << " is now unavailable.\n";
			flag = 1;
			}
		}
		else{
			cout << "The movie is not on the list.\n";
		}
		qtt++;
		ctr = 2;
		onTheList = false;
	}
	else if (choice == 2){
		cout << "Movie list\n";
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ". " << movies[i][0];
			if (movies[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Unavailable)\n";
			}
		}
		cout << "\nWhat is the title of the movie you'll return? ";
		cin.get();
		getline(cin, title);
		for (int i = 0; i <= 24; i++){
			if(movies[i][0] == title){
				movies[i][4] = "yes";
				onTheList = true;
				ctr = i;
			}
		}
		if (onTheList == 1){
			cout << "The movie " << movies[ctr][0];
			if(movies[ctr][4] == "yes" && flag == 0){
			cout << " is available.\n";
			}
			else{
			cout << " is now available.\n";
			flag = 0;
			}
		}
		else{
			cout << "The movie is not on the list.\n";
		}
		ctr = 2;
		onTheList = false;
	}
	else if (choice == 3){
		cout << "Search: ";
		cin.get();
		getline(cin, title);
		for (int i = 0; i <= 24; i++){
			if(movies[i][0] == title){
				cout << "\nMovie Title: " << movies[i][0] << endl;
				cout << "Director: " << movies[i][1] << endl;
				cout << "Producer: " << movies[i][2] << endl;
				cout << "Cast: " << movies[i][3] << endl;
				cout << "Availability: ";
				if (movies[i][4] == "yes"){
					cout << "Available\n";
				}
				else{
					cout << "Unavailable\n";
				}
				onTheList = true;
			}
		}
		if(onTheList != 1){
				cout << "The movie is not on the list.\n";
			}
		onTheList = false;
	}
	else if (choice == 4){
		cout << "Movie list\n";
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ". " << movies[i][0];
			if (movies[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Unavailable)\n";
			}
		}
	}
	else if (choice == 5){
		cout << "Name: " << fName << " " << lName << endl;
		cout << "Date of Transaction: " << dateRented << endl;
		if (flag == 1){
			cout << "\nMovies Rented:" << "x" << qtt << endl;
			for (int i = 0; i <= 24; i ++){
				if (movies[i][4] == "no"){
					cout << movies[i][0] << endl;
				}
			}
		}
		else{
				cout << "\nThere is nothing on your cart!" << endl;
		}
		cout << "\n0: Pay Out";
	}
	else if (choice == 6){
		return 2;
	}
	return options();
}

string giveName(string whatName){
	string name;
	cout << whatName << " name: ";
	cin >> name;
	return name;
}

int options(){
	int choice;
	cout << "\n1: Return to options\n2: Exit\n>> ";
	cin >> choice;
	system("CLS");
	return choice;
}